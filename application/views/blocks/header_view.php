
<section id="home">
<!--Loader  -->
        <div class="loader"><i class="fa fa-refresh fa-spin"></i></div>
        <!--LOader end  -->
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            <header>
                <!-- Header inner  -->
                <div class="header-inner">
                    <!-- Logo  -->
                    <div class="logo-holder">
                        <a href="index.html"><img src="images/logo.png" alt=""></a>
                    </div>
                    <!--Logo end  -->
                    <!--Navigation  -->
                    <div class="nav-button-holder">
                        <div class="nav-button vis-m"><span></span><span></span><span></span></div>
                    </div>
                    <div class="show-share isShare">Share</div>
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li><a href="index.html" class="act-link">Home</a></li>
                                <li><a href="portfolio.html">Portfolio</a></li>
                                <li><a href="about.html">About us </a>
                                </li>
                                <li><a href="contact.html">Contact</a></li>
                                <!-- <li><a href="blog.html">Blog</a></li> -->
                                <!-- <li>
                                    <a>Pages</a>
                                    <ul>
                                        <li>
                                            <a>Home</a>
                                            <ul>
                                                <li><a href="index.html">Slider</a></li>
                                                <li><a href="index2.html">Slideshow</a></li>
                                                <li><a href="index3.html">Multi Slider</a></li>
                                                <li><a href="index4.html">Video</a></li>
                                                <li><a href="index5.html">horizontal</a></li>
                                                <li><a href="index6.html">Image</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="aboutme.html">About me</a></li>
                                        <li><a href="blog-single.html">Blog single</a></li>
                                        <li><a href="contact2.html">Contact 2</a></li>
                                        <li>
                                            <a>Portfolio</a>
                                            <ul>
                                                <li><a href="portfolio.html">horizontal</a></li>
                                                <li><a href="portfolio2.html">Masonry</a></li>
                                                <li><a href="portfolio3.html">Grid 3</a></li>
                                                <li><a href="portfolio4.html">Grid 4</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a>Portfolio single</a>
                                            <ul>
                                                <li><a href="portfolio-single.html">Style 1</a></li>
                                                <li><a href="portfolio-single2.html">Style 2</a></li>
                                                <li><a href="portfolio-single3.html">Style 3</a></li>
                                                <li><a href="portfolio-single4.html">Style 4</a></li>
                                                <li><a href="portfolio-single5.html">Style 5</a></li>
                                                <li><a href="portfolio-single6.html">Style 6</a></li>
                                                <li><a href="portfolio-single7.html">Style 7</a></li>
                                                <li><a href="portfolio-single8.html">Style 8</a></li>
                                                <li><a href="portfolio-single9.html">Style 9</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="404.html">404</a></li>
                                    </ul>
                                </li> -->
                            </ul>
                        </nav>
                    </div>
                    <!--navigation end -->
                </div>
                <!--Header inner end  -->
            </header>
            <!--header end -->
</section>
