<!--=============== Footer ===============-->
            <footer>
                <div class="policy-box">
                    <span>&#169; Elisabeth Adam 2016 . All rights reserved. </span>
                    <ul>
                        <li><a href="mailto: contact@elisabethadam.com">contact@elisabethadam.com</a></li>
                        <li><a href="callto:+40751121942">+4 (0751) 121 942</a></li>
                    </ul>
                </div>
                <!-- footer social -->
                <div class="footer-social">
                    <ul>
                        <li><a href="https://www.facebook.com/elisabethadamcom" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                    </ul>
                </div>
                <!-- footer social end -->
                <div class="to-top"><i class="fa fa-angle-up"></i></div>
            </footer>
            <!-- footer end -->
