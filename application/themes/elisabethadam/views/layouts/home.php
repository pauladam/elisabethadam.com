<!DOCTYPE html>
<html>

<?php $this->load->view('blocks/elisabethadam/head_view'); ?>
<body onresize="myFunction()">
<!-- START BODY TAG -->

<?php $this->load->view('blocks/elisabethadam/header_parallax'); ?>
<?php $this->load->view('blocks/elisabethadam/puncte_tari'); ?>
<?php $this->load->view('blocks/elisabethadam/galerie'); ?>
<?php $this->load->view('blocks/elisabethadam/welcome'); ?>
<?php $this->load->view('blocks/elisabethadam/meniu'); ?>
<?php $this->load->view('blocks/elisabethadam/about'); ?>
<?php $this->load->view('blocks/elisabethadam/rezervari'); ?>
<section id="contact_area">
	<?php $this->load->view('blocks/elisabethadam/contact_view'); ?>
	<?php $this->load->view('blocks/elisabethadam/footer_view'); ?>
</section>
<?php $this->load->view('blocks/elisabethadam/scripts'); ?>
</body>

</html>
